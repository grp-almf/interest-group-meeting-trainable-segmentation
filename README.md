# interest-group-meeting-trainable-segmentation

## Current state

### Features => Random Forest

Pro:
- Sparse labelling 
- Fast training (seconds - minutes)
- RF generalises well

Con:
- No iterative training => Takes only limited amount of training data...
    - May be possible to implement...(online RF)
- Slow during predicting (no GPU implementations)


#### Implementations

Ilastik 
- Kreshuk et al.
- Python
- UI
- Big image data compatible

Trainable Weka Segmentation (TWS)
- Ignacio et al.
- Java
- Fiji ImageJ1 UI
- No big image data

Context aware trainable segmentation (CATS)
- Based on TWS
- Tischi
- Java
- Fiji ImageJ1 UI
- Deeper features
- Big image data compatible
- Seamless Slurm cluster integration

LabKit
- Jug et al.
- Fiji BigDataViewer UI
- Big image data compatible


### U-Net (Ronneberger et al.)

Pro:
- Unlimited (iterative) training data
- Deep features 
- Self learned features
- Non-linear features
- "Mixed convolutional features" in deep layers
- Fast classification (due to GPU implementations)

Con:
- Dense labelling required
- Slow learning (hours)
- Learning takes expertiese (network parameter tuning)
- Standart PCs might not have a suitable GPU


#### Implementations

- Python code
    - PyTorch (go to Kreshuk lab)
    - some Google stuff...

- CellProfiler 2D Nucleus Segmentation (also Python) ??

## Challenges

- Transfer learning
    - How different can the input data for one classifier be?
    - How well can you generalise the classification
- U-Net with sparse labelling
- U-Net network parameter tuning..
- Proofreading tools
- Timelapse (tracking) integration

